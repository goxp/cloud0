package common

const (
	HeaderXRequestID = "x-request-id"
	HeaderUserID     = "x-user-id"
	HeaderUserMeta   = "x-user-type"
	HeaderTenantID   = "x-tenant-id"
	HeaderXForwarded = "x-forwarded-for"
	HeaderSupervisor = "x-supervisor-id"
)
