package text

import (
	"encoding/json"
	"fmt"
	"strings"
	"time"
)

type Stringer interface {
	String() string
}

func Stringify(v interface{}) string {
	if s, ok := v.(Stringer); ok {
		return s.String()
	}

	switch v.(type) {
	case time.Time:
		return v.(time.Time).Format(time.RFC3339)
	}

	bytes, err := json.Marshal(v)
	if err == nil {
		return string(bytes)
	}

	return fmt.Sprintf("%v", v)
}

func SerializeSlice[T any](slice []T) string {
	var result strings.Builder
	for i, item := range slice {
		result.WriteString(Stringify(item))
		if i < len(slice)-1 {
			result.WriteString(",")
		}
	}
	return result.String()
}

// unmarshalSliceFromStr converts a string-list-values to target slice
// e.g. "1,2,3" -> []Priority{1, 2, 3}
func unmarshalSliceFromStr[T any](raw string) []T {
	var result []T
	items := strings.Split(raw, ",")
	for _, item := range items {
		var t T
		if err := json.Unmarshal([]byte(item), &t); err != nil {
			// skip unmarshal-able value
			continue
		}
		result = append(result, t)
	}
	return result
}
