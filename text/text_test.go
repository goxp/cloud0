package text

import (
	"reflect"
	"testing"
)

func TestSerializeSlice(t *testing.T) {
	type args[T any] struct {
		slice []T
	}
	type testCase[T any] struct {
		name string
		args args[T]
		want string
	}
	tests := []testCase[uint64]{
		{
			name: "uint64",
			args: args[uint64]{
				slice: []uint64{1, 2, 3},
			},
			want: "1,2,3",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := SerializeSlice(tt.args.slice); got != tt.want {
				t.Errorf("SerializeSlice() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestStringify(t *testing.T) {
	type args struct {
		v interface{}
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "uint64",
			args: args{
				v: uint64(1),
			},
			want: "1",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Stringify(tt.args.v); got != tt.want {
				t.Errorf("Stringify() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_unmarshalSliceFromStr(t *testing.T) {
	type args struct {
		raw string
	}
	type testCase[T any] struct {
		name string
		args args
		want []T
	}
	tests := []testCase[uint64]{
		{
			name: "uint64",
			args: args{
				raw: "1,2,3",
			},
			want: []uint64{1, 2, 3},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := unmarshalSliceFromStr[uint64](tt.args.raw); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("unmarshalSliceFromStr() = %v, want %v", got, tt.want)
			}
		})
	}
}
