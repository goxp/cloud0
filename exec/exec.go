package exec

import (
	"context"
	"fmt"
	"github.com/sirupsen/logrus"
	"gitlab.com/goxp/cloud0/logger"
	"gitlab.com/goxp/cloud0/text"
	"time"
)

type CommandHandler[T2 any, T3 any] interface {
	Handle(ctx context.Context, cmd T2) (T3, error)
}

func Invoke[T2 any, T3 any](ctx context.Context, handler CommandHandler[T2, T3], cmd T2) (result T3, err error) {
	log := logger.WithCtx(ctx, fmt.Sprintf("commandCaller[%T]", handler))
	start := time.Now()

	defer func() {
		fields := logrus.Fields{
			"duration": time.Since(start).Milliseconds(),
			"params":   text.Stringify(cmd),
		}

		if err != nil {
			fields["error"] = err.Error()
		} else {
			fields["result"] = text.Stringify(result)
		}

		log.WithFields(fields).Infof("command [%T] executed", handler)
	}()

	result, err = handler.Handle(ctx, cmd)
	return
}
