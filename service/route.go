package service

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/goxp/cloud0/ginext"
)

type AuthType byte

const (
	AuthTypePub AuthType = 1
	AuthTypeJWT AuthType = 2
)

func (app *BaseApp) AddRoute(method string, pattern string, auth AuthType, handlers ...ginext.Handler) {
	ginHandlers := gin.HandlersChain{}
	if auth == AuthTypeJWT {
		ginHandlers = append(ginHandlers, ginext.AuthRequiredMiddleware)
	}
	for _, h := range handlers {
		ginHandlers = append(ginHandlers, ginext.WrapHandler(h))
	}
	app.Router.Handle(method, pattern, ginHandlers...)
}
