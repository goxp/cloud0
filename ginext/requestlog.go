package ginext

import (
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/goxp/cloud0/common"
	"gitlab.com/goxp/cloud0/logger"
)

func AccessLogMiddleware(env string) gin.HandlerFunc {
	l := logger.WithField("env", env)
	extractHeaders := []string{
		common.HeaderXForwarded,
		common.HeaderTenantID,
		common.HeaderUserID,
		common.HeaderXRequestID,
		common.HeaderSupervisor,
	}

	return func(c *gin.Context) {

		if c.Request.URL.Path == "/status" {
			c.Next()
			return
		}

		start := time.Now()
		path := c.Request.URL.Path
		raw := c.Request.URL.RawQuery
		if raw != "" {
			path = path + "?" + raw
		}

		defer func() {
			latency := time.Since(start).Milliseconds()
			l = l.
				WithField("status", c.Writer.Status()).
				WithField("method", c.Request.Method).
				WithField("path", path).
				WithField("ip", c.ClientIP()).
				WithField("latency", latency).
				WithField("user-agent", c.Request.UserAgent())

			for _, header := range extractHeaders {
				if v := c.GetHeader(header); v != "" {
					l = l.WithField(header, v)
				}
			}

			// log extra fields from context
			for k, v := range c.Keys {
				if strings.HasPrefix(k, "log_") {
					key := strings.TrimPrefix(k, "log_")
					l = l.WithField(key, v)
				}
			}

			msg := "access log"
			if msg_ := c.GetString("accessLogMsg"); msg_ != "" {
				msg = msg_
			}

			l.Info(msg)
		}()

		c.Next()
	}
}
