package ginext

// ResponseBody ...
type ResponseBody interface{}

type HasStatusCodeResponse interface {
	WithResponseCode(code string) ResponseBody
}
