package ginext

import (
	"context"

	"github.com/gin-gonic/gin"
)

const (
	RequestIDName = "x-request-id"
)

// FromGinRequestContext makes a new context from Gin request context, copy x-request-id to if any
// use request context instead of gin context to handle user cancelling
func FromGinRequestContext(c *gin.Context) context.Context {
	ctx := c.Request.Context()
	if requestID := c.GetString(RequestIDName); requestID != "" {
		ctx = context.WithValue(ctx, RequestIDName, requestID)
	} else if requestID = c.GetHeader(RequestIDName); requestID != "" {
		ctx = context.WithValue(ctx, RequestIDName, requestID)
	}
	return ctx
}
