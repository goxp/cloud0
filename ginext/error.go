package ginext

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-errors/errors"
	"gitlab.com/goxp/cloud0/logger"
)

var (
	_ ApiError = &apiErr{}
)

// ApiError is an interface that supports return Code & Marshal to json
type ApiError interface {
	Code() int
	MarshalJSON() ([]byte, error)
	ResponseCode() string
}

type ErrResponse struct {
	Code   string `json:"code,omitempty"`
	Detail string `json:"detail"`
}

type apiErr struct {
	code         int
	message      string
	responseCode string
}

func (e *apiErr) Code() int {
	return e.code
}

func (e *apiErr) MarshalJSON() ([]byte, error) {
	resp := &ErrResponse{
		Code:   e.ResponseCode(),
		Detail: e.Error(),
	}
	return json.Marshal(resp)
}

func (e *apiErr) Error() string {
	return e.message
}

func (e *apiErr) ResponseCode() string {
	return e.responseCode
}

func NewError(code int, message string) error {
	return &apiErr{code: code, message: message}
}

func NewErrorCode(httpCode int, message string, respCode string) error {
	return &apiErr{code: httpCode, message: message, responseCode: respCode}
}

func CreateErrorHandler(printStacks ...bool) gin.HandlerFunc {
	printStack := false
	if len(printStacks) > 0 {
		printStack = printStacks[0]
	}

	return func(c *gin.Context) {
		l := logger.WithCtx(c, "ErrorHandler")

		var (
			err      error
			respCode = "500"
		)

		defer func() {
			if r := recover(); r != nil {
				switch v := r.(type) {
				case error:
					err = v
				default:
					err = NewError(http.StatusInternalServerError, fmt.Sprintf("unexpected error: %v", v))
				}
			}

			// no error
			if err == nil && len(c.Errors) == 0 {
				return
			}

			if err == nil && len(c.Errors) > 0 {
				err = c.Errors.Last().Err
			}

			l.Errorf("handle request error: %s", err)

			code := http.StatusInternalServerError
			if v, ok := err.(ApiError); ok {
				code = v.Code()
				respCode = v.ResponseCode()
			} else if v, ok := err.(*json.UnmarshalTypeError); ok {
				code = http.StatusBadRequest
				respCode = "400"
				err = &validationErrors{
					fieldErrors: []ValidatorFieldError{
						&validatorFieldError{
							field:   v.Field,
							message: fmt.Sprintf("invalid type `%s`, requires `%s`", v.Value, v.Type.String()),
						},
					},
				}
			} else {
				// only print stack when getting unexpected error
				if printStack {
					fmt.Println(errors.Wrap(err, 1).ErrorStack())
				}
			}

			c.JSON(code, &GeneralBody{Error: err, Code: respCode})
		}()

		c.Next()
	}
}
