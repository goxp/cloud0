package ginext

// BodyMeta represents a body meta data like pagination or extra response information
// it should always be rendered as a map of key: value
type BodyMeta map[string]interface{}

// GeneralBody defines a general response body
type GeneralBody struct {
	Data   interface{} `json:"data,omitempty"`
	Meta   BodyMeta    `json:"meta,omitempty"`
	Error  interface{} `json:"error,omitempty"`
	Status interface{} `json:"status,omitempty"`
	Code   interface{} `json:"code,omitempty"`
}

func NewBody(data interface{}, meta BodyMeta) ResponseBody {
	return &GeneralBody{
		Data: data,
		Meta: meta,
	}
}

func NewBodyPaginated(data interface{}, pager *Pager) ResponseBody {
	return &GeneralBody{
		Data: data,
		Meta: BodyMeta{
			"page":        pager.GetPage(),
			"total_pages": pager.GetTotalPages(),
			"page_size":   pager.GetPageSize(),
			"total":       pager.TotalRows,
			"pages":       pager.GetTotalPages(),
		},
	}
}

func (b *GeneralBody) WithResponseCode(code string) *GeneralBody {
	b.Code = code
	return b
}
