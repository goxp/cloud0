package logger

import (
	"context"
	"gitlab.com/goxp/cloud0/common"
	"os"
	"sync"
	"time"

	"github.com/sirupsen/logrus"
)

var (
	DefaultLogger    *logrus.Logger
	DefaultBaseEntry *logrus.Entry
	initOnce         sync.Once
)

var (
	LogLevelEnv   = "LOG_LEVEL"
	LogFormatEnv  = "LOG_FORMAT"
	LogFormatJSON = "json"

	LogTextFormatter = &logrus.TextFormatter{
		ForceColors:               false,
		DisableColors:             false,
		ForceQuote:                false,
		DisableQuote:              true,
		EnvironmentOverrideColors: false,
		DisableTimestamp:          false,
		FullTimestamp:             false,
		TimestampFormat:           time.RFC3339,
		DisableSorting:            false,
		SortingFunc:               nil,
		DisableLevelTruncation:    false,
		PadLevelText:              false,
		QuoteEmptyFields:          false,
		FieldMap:                  nil,
		CallerPrettyfier:          nil,
	}

	LogJSONFormat = &logrus.JSONFormatter{TimestampFormat: time.RFC3339,
		DisableTimestamp: false,
		DataKey:          "",
		FieldMap:         nil,
		CallerPrettyfier: nil,
		PrettyPrint:      false,
	}

	LogDefaultOutput = os.Stdout
)

// GetStringer describe an object that has capacity to return a string via GetString
// It uses as Gin Context in case we want to cut off gin dependency here
type GetStringer interface {
	GetString(key string) string
}

func Init(name string) {
	initOnce.Do(func() {
		DefaultLogger = logrus.New()
		if l, e := logrus.ParseLevel(os.Getenv(LogLevelEnv)); e == nil {
			DefaultLogger.SetLevel(l)
		}
		DefaultLogger.SetFormatter(LogTextFormatter)

		if os.Getenv(LogFormatEnv) == LogFormatJSON {
			DefaultLogger.SetFormatter(LogJSONFormat)
		}

		DefaultLogger.SetOutput(LogDefaultOutput)
		DefaultBaseEntry = DefaultLogger.WithField("service", name)
	})
}

// Tag sets a tag name then returns a log entry ready to write
func Tag(tag string) *logrus.Entry {
	if DefaultBaseEntry == nil {
		Init("common")
	}
	return DefaultBaseEntry.WithField("tag", tag)
}

// TagWithGetString return a log entry from tag name & x-request-id in Gin context if has
func TagWithGetString(tag string, ctx GetStringer) *logrus.Entry {
	l := Tag(tag)
	if requestID := ctx.GetString(common.HeaderXRequestID); requestID != "" {
		l = l.WithField(common.HeaderXRequestID, requestID)
	}
	return l
}

func WithCtx(ctx context.Context, tag string) *logrus.Entry {
	l := Tag(tag)
	if requestID, ok := ctx.Value(common.HeaderXRequestID).(string); ok && requestID != "" {
		l = l.WithField(common.HeaderXRequestID, requestID)
	}
	return l
}

func WithField(key string, value interface{}) *logrus.Entry {
	return DefaultBaseEntry.WithField(key, value)
}
